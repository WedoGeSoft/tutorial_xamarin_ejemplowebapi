﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using EjemploWebApi.Models;

namespace EjemploWebApi.Controllers
{
    // Ruta del controlador: mi.dominio.com/api/Personas
    public class PersonasController : ApiController
    {
        public PersonasController()
        {
            if (RepositorioPersonas.SeleccionarTodos().Count() <= 0)
            {
                RepositorioPersonas.Agregar(new Persona
                {
                    Nombre = "Salvador",
                    Apellidos = "Ramirez Navarro",
                    Teléfono = "3300000001"
                });

                RepositorioPersonas.Agregar(new Persona
                {
                    Nombre = "Fausto",
                    Apellidos = "Favela",
                    Teléfono = "3300000002"
                });

                RepositorioPersonas.Agregar(new Persona
                {
                    Nombre = "Omar",
                    Apellidos = "García Espíritu",
                    Teléfono = "3300000003"
                });
            }
        }

        // GET: mi.dominio.com/api/Personas     -- Retornará todos los registros
        // GET: mi.dominio.com/api/Personas/1   -- Retornará el registro cuyo id sea 1 
        public IEnumerable<Persona> Get(int id = 0)
        {
            if (id == 0)
                return RepositorioPersonas.SeleccionarTodos();

            Persona encontrado = RepositorioPersonas.SeleccionarPorId(id);

            if (encontrado == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            // Retornar una lista con un único elemento
            return new List<Persona> { encontrado };
        }

        public HttpResponseMessage Post(Persona nuevaPersona)
        {
            Persona personaEncontrada = RepositorioPersonas.SeleccionarPorId(nuevaPersona.Id);

            try
            {
                if (personaEncontrada == null)
                {
                    RepositorioPersonas.Agregar(nuevaPersona);
                    return Request.CreateResponse(HttpStatusCode.Created, nuevaPersona);
                }
                else
                {
                    RepositorioPersonas.Actualizar(nuevaPersona);
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                }
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(HttpStatusCode.InternalServerError);
            }
        }
    }
}
