﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EjemploWebApi.Models
{
    public class Persona
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Apellidos { get; set; }
        public string Teléfono { get; set; }
    }
}