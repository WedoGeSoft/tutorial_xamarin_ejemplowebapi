﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EjemploWebApi.Models
{
    public static class RepositorioPersonas
    {
        private static List<Persona> personas { get; set; } = new List<Persona>();

        public static Persona Agregar(Persona nuevaPersona)
        {
            int maxId = 0;

            if (personas.Count > 0)
            {
                maxId = personas.OrderByDescending(p => p.Id).First().Id;
            }

            nuevaPersona.Id = maxId + 1;
            personas.Add(nuevaPersona);

            return nuevaPersona;
        }
        public static List<Persona> SeleccionarTodos()
        {
            return personas;
        }

        public static Persona SeleccionarPorId(int id)
        {
            return (from persona in personas
                    where persona.Id == id
                    select persona).FirstOrDefault();
        }

        public static bool Actualizar(Persona persona)
        {
            Persona encontrada = SeleccionarPorId(persona.Id);

            if (encontrada == null)
                return false;

            encontrada.Nombre = persona.Nombre;
            encontrada.Apellidos = persona.Apellidos;
            encontrada.Teléfono = persona.Teléfono;

            return true;
        }

        public static bool Eliminar(Persona persona)
        {
            Persona encontrada = SeleccionarPorId(persona.Id);

            if (encontrada == null)
                return false;

            return personas.Remove(encontrada);
        }
    }
}